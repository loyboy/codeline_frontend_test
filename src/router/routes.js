export default [
    {
        path: '/',
        component: require('@/components/Layout/Master').default,
        meta: {
            pageTitle: 'Codeline Weather App from Loyboy'
         },
        children: [
            {
                path: '',
                name: 'home',
                component: require('@/components/Home').default,
                meta: {
                    pageTitle: 'Codeline Weather Deatils for Today'
                }
            },
            {
                path: '/weather/:woeid',
                name: 'weather',
                component: require('@/components/Detail').default,
                meta: {
                    pageTitle: 'Codeline Weather Detail'
                }
            },
            {
                path: '/search/:keyword',
                name: 'search',
                component: require('@/components/Search').default,
                meta: {
                    pageTitle: 'Codeline Weather Search'
                }
            },
        ]
    },
    { path: '*', redirect: '/' }
  ]
  