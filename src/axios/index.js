import axios from 'axios'

var instance = axios.create({
  baseURL: 'http://localhost:8080/weather/index.php',
  headers: { 
    'X-Requested-With': 'XMLHttpRequest',
    'Access-Control-Allow-Origin' : '*'
  }
})

export default instance
