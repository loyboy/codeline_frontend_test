import Vue from 'vue'
import App from './App.vue'
import router from './router'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'
locale.use(lang)

// import 'bootstrap/dist/js/bootstrap'
import 'bootstrap';

// import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/scss/bootstrap.scss'

Vue.use(ElementUI);

Vue.component('form-error', require('./components/Partials/FormError.vue').default)
Vue.component('weather', require('./components/Partials/Weather.vue').default)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
