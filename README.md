# codeline_frontend_test

## Project setup
```
npm install
```

### Compiles and live-reloads for development purposes
```
npm run serve
```

### Compiles and minifies for production in a build 
```
npm run build
```

### Run your tests for error viewing
```
npm run test
```

